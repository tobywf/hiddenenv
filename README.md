Hiddenenv
=========

Out of sight, out of mind
-------------------------

The idea behind hiddenenv is similar to [virtualenvwrapper](https://virtualenvwrapper.readthedocs.org) or [virtualfish](https://virtualfish.readthedocs.org), but simpler (and therefore more limited).

The idea is that most projects for which I'd want an auto-activating virtualenv are long-lived, uniquely named git repositories. So why not create a virtualenv based on the repository name? Then, when you jump into the git repo, the corresponding virtualenv can automatically be activated.

To create a "hidden" virtualenv:

```bash_session
$ cd ~/code/project/
$ ls -a
.        ..       .git     project  setup.py
$ hiddenenv create
[...]
$ which python
/home/user/.virtualenvs/project/bin/python
```

Pros:

- No extra cruft, unlike virtualfish's auto-activation file.
- No need to remember what the virtualenv is called.
- No more virtualenvs floating around in the source tree, which isn't an issue with the correct `.gitignore`, but really annoying for e.g. Dropbox or Google Drive.
- Doesn't mess with your prompt, this is what themes are for.

Cons:

- Have to use git.
- Potential for confusion if projects are not uniquely named.
- Might be left with orphan virtualenvs.
- Script runs everytime you change directory.
- Written by someone who isn't a fish shell master.
- Doesn't mess with your prompt.
