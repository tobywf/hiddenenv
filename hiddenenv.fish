# set default location
if not set -q HIDDENENV_HOME
    set -g HIDDENENV_HOME $HOME/.virtualenvs
end

function hiddenenv --description "Hiddenenv: Move virtualenvs outside the source tree"
    __hiddenenvfn_update

    if not set -q HIDDENENV_NAME
        echo "Not a git repository"
        return
    end

    set -l funcname "__hiddenenv_$argv[1]"
    if test (count $argv) -gt 1
        set args $argv[2..-1]
    else
        set args
    end

    if functions -q $funcname
        eval $funcname $args
    else
        echo "Invalid command $argv[1]"
        return
    end
end

function __hiddenenv_create --description "Create a new virtualenv"
    if test -d "$HIDDENENV_PATH"
        echo "Virtualenv $HIDDENENV_PATH already exists."
        return 2
    end
    virtualenv $argv $HIDDENENV_PATH
    set vestatus $status
    if begin; test $vestatus -eq 0; and test -d "$HIDDENENV_PATH"; end
        __hiddenenv_activate
    else
        echo "Error: virtualenv returned status $vestatus."
        return $vestatus
    end
end

function __hiddenenv_delete --description "Delete a virtualenv"
    if begin; set -q VIRTUAL_ENV; and test "$HIDDENENV_PATH" = "$VIRTUAL_ENV"; end
        __hiddenenv_deactivate
    end
    echo "Removing $HIDDENENV_PATH"
    rm -rf "$HIDDENENV_PATH"
end

function __hiddenenv_deactivate --description "Deactivate the current virtualenv"
    if not set -q VIRTUAL_ENV
        return
    end

    # basically ripped straight from activate.fish

    # reset old environment variables
    if test -n "$_OLD_VIRTUAL_PATH"
        set -gx PATH $_OLD_VIRTUAL_PATH
        set -e _OLD_VIRTUAL_PATH
    end
    if test -n "$_OLD_VIRTUAL_PYTHONHOME"
        set -gx PYTHONHOME $_OLD_VIRTUAL_PYTHONHOME
        set -e _OLD_VIRTUAL_PYTHONHOME
    end
    set -e VIRTUAL_ENV
end

function __hiddenenv_activate --description "Activate the current virtualenv"
    echo "Activate"

    if not test -d "$HIDDENENV_PATH"
        echo "Virtualenv for $HIDDENENV_NAME doesn't exists."
        return 2
    end

    # basically ripped straight from activate.fish

    # unset irrelevant variables
    __hiddenenv_deactivate

    set -gx VIRTUAL_ENV $HIDDENENV_PATH

    set -gx _OLD_VIRTUAL_PATH $PATH
    set -gx PATH "$VIRTUAL_ENV/bin" $PATH

    # unset PYTHONHOME if set
    if set -q PYTHONHOME
        set -gx _OLD_VIRTUAL_PYTHONHOME $PYTHONHOME
        set -e PYTHONHOME
    end
end

function __hiddenenvfn_update
    if command git rev-parse --is-inside-work-tree >/dev/null ^/dev/null
        set -g HIDDENENV_NAME (basename (command git rev-parse --show-toplevel ^/dev/null))
        set -g HIDDENENV_PATH $HIDDENENV_HOME/$HIDDENENV_NAME
    else
        set -ge HIDDENENV_NAME
        set -ge HIDDENENV_PATH
    end
end

function __hiddenenvfn_auto_activate --on-variable PWD
    if status --is-command-substitution
        return
    end

    __hiddenenvfn_update

    # no git repo? no hidden env
    if not set -q HIDDENENV_NAME
        # TODO: deactivate on leave
        return
    end

    # correct virtualenv already activated?
    if begin; set -q VIRTUAL_ENV; and test "$HIDDENENV_PATH" = "$VIRTUAL_ENV"; end
        return
    end

    # virtualenv exists?
    if test -d "$HIDDENENV_PATH"
        __hiddenenv_activate
    end
end
